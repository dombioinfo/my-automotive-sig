# Automotive SIG

This is the central repository for the Automotive SIG. It contains recipes for
building the images, information on how to download them, and
documentation.

For more information, visit:

* [Automotive SIG wiki page] (https://wiki.centos.org/SpecialInterestGroup/Automotive)
* [Automotive SIG documentation] (https://sigs.centos.org/automotive)


**Archived**

This repository has been archived. Its content has been split into two
repositories:

* The Automotive SIG's documentation: [https://gitlab.com/CentOS/automotive/sig-docs](https://gitlab.com/CentOS/automotive/sig-docs)
* The Automotive SIG's sample images: [https://gitlab.com/CentOS/automotive/sample-images](https://gitlab.com/CentOS/automotive/sample-images)
